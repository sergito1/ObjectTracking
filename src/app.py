import json
import os
import shutil
from urllib import response

import cv2
from dotenv import load_dotenv
from flask import Flask, Response, redirect, render_template, request, url_for
from prometheus_flask_exporter import PrometheusMetrics

from tracking import detect_object, read_video

tmpl_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), "templates")

app = Flask(__name__, template_folder=tmpl_dir)
metrics = PrometheusMetrics(app)

metrics.info("app_info", "Object Tracking", version="1.0.0")

UPLOAD_DIR = "./data"


def generate_first_frame(path):
    camera = read_video(path)
    success, frame = camera.read()
    if not success:
        return None

    _, buffer = cv2.imencode(".jpg", frame)

    frame = buffer.tobytes()
    return frame


@app.route("/")
def index():
    return render_template("index.html")


@app.route("/", methods=["GET", "POST"])
def upload_file():
    if request.method == "POST":
        request_file = request.files.get("file")
        id_video = request.form["id_video"]
        dir_id = f"{UPLOAD_DIR}/{id_video}"
        try:
            shutil.rmtree(dir_id)
        except OSError:
            pass
        os.makedirs(dir_id, exist_ok=True)
        path = f"{dir_id}/video.mp4"
        request_file.save(path)
        first_frame = generate_first_frame(path)

        with open(f"{dir_id}/preview.jpg", "wb") as file:
            file.write(first_frame)

        return redirect(url_for("index", id_video=id_video))
    return redirect(url_for("index"))


def get_preview(id_video):
    preview_file = f"{UPLOAD_DIR}/{id_video}/preview.jpg"
    with open(preview_file, "rb") as f:
        frame = f.read()
    return b"--frame\r\n" b"Content-Type: image/jpeg\r\n\r\n" + frame + b"\r\n"


@app.route("/preview", methods=["GET", "POST"])
def preview():
    row_id = request.args.get("id_video")
    print(row_id)
    return Response(
        get_preview(row_id),
        mimetype="multipart/x-mixed-replace; boundary=frame",
    )


@app.route("/video", methods=["GET", "POST"])
def video():
    row_id = request.args.get("id_video")
    rect_coords = json.loads(request.args.get("rect_coords"))
    video_file = f"{UPLOAD_DIR}/{row_id}/video.mp4"
    return Response(
        detect_object(video_file, rect_coords),
        mimetype="multipart/x-mixed-replace; boundary=frame",
    )


if __name__ == "__main__":
    load_dotenv("./.env")
    app.run(host="0.0.0.0")
